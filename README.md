# GO WITH THE FLOW - TEAM 105

## Team Members
- 20WH1A05B6 : B ALEKHYA : CSE
- 21WH5A0501 : M ANISHA : CSE
- 21WH5A0502 : T HEMA : CSE
- 20WH1A05E9 : K AKSHAYA : CSE
- 20WH1A05H8 : M SREEYA : CSE
- 20WH1A1287 : E ARUNA : IT

## Description of the problem statement

- A celebrated river authority Flo Ng, wants her new book on the rivers of the world to include the longest rivers possible.

- In this context, River stands for sequence of white spaces formed by the gaps between the words that extends down several lines. The text should be in mono-spaced font and aligned to left. The position of the space should differ by atmost one position of the space in the ine above it.

### Input 
    First line consists of an integer n(2 <= n <= 2500)
    Second line consists of texts
### Output 
    Width of the longest river possible followed by the length of longest river

## Contributions (Day Wise)

### Day 1 (20/10/2021)

    
- 20WH1A05B6 : LaTex installation and ppt   
- 20WH5A0501 : Worked on input and output of problem statement    
- 20WH5A0502 : Analysed approach of the problem statement
- 20WH1A05E9 : Presentation  
-  20WH1A05H8 : GitLab
-  20WH1A1287 : Gathering information
